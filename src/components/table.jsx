import { Container, Grid, Button, TextField, Typography, TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { addlist, inputAction } from "../actions/table.action"
// ,Table, TableHead, TableContainer, TableRow, TableCell, TableBody, Paper
const TableComponent = () => {
    // useSelector để đọc state từ redux
    const { input, tableList } = useSelector((reduxData) => {
        return reduxData.tableReducer
    })
    // khai báo hàm dispatch sự kiện tới redux của store
    const dispatch = useDispatch()
    const onChangeInput = (event) => {
        dispatch(inputAction(event.target.value))
    }

    const addListTable = () => {
        dispatch(addlist())
    }

    return (
        <Container>
            <Grid container mt={5} mb={5} spacing={2} alignItems='center'>
                <Grid item sm={2}>
                    <Typography variant="h6" gutterBottom>
                        Nhập nội dung
                    </Typography>
                </Grid>
                <Grid item sm={8} >
                    <TextField id="outlined-basic" label="Filter" variant="outlined" fullWidth value={input} onChange={onChangeInput}/>

                </Grid>
                <Grid item sm={2}>
                    <Button variant="outlined" onClick={addListTable}>Add Task</Button>
                </Grid>
            </Grid>
            <TableContainer>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell align="left">STT</TableCell>
                            <TableCell align="center">Nội dung</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                            {
                                tableList.map((element, index) => {
                                    return (
                                        <TableRow key={index}>
                                            <TableCell align="left">{index}</TableCell>
                                            <TableCell align="center">{element.row}</TableCell>
                                        </TableRow>
                                    )
                                })
                            }
                            
                    </TableBody>
                </Table>
            </TableContainer>

        </Container>

    )
}


export default TableComponent