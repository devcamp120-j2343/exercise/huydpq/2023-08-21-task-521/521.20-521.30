import './App.css';
import TableComponent from './components/table';
import "bootstrap/dist/css/bootstrap.min.css"

function App() {
  return (
    <div>
      <TableComponent/>
    </div>
  );
}

export default App;
