import { ADD_LIST_CLICK, INPUT_TEXT } from "../constants/table.constant"

export const inputAction = (value) => {
    return {
        type: INPUT_TEXT,
        payLoad: value

    }
}

export const addlist = () => {
    return {
        type: ADD_LIST_CLICK,
    }
}