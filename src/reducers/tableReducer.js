// định nghĩa state khởi tạo cho task

import { ADD_LIST_CLICK, INPUT_TEXT } from "../constants/table.constant";


const initialState = {
    // input: "teet",
    // taskList: [{
    //     taskName: "task1",
    // }]
    input: "",
    tableList: []
}

const tableReducer = (state = initialState, action) => {
    switch (action.type) {
        case INPUT_TEXT:
            state.input = action.payLoad
            break;
        case ADD_LIST_CLICK:
            state.tableList.push({
                row: state.input
            })
            break;
        default:
            break;
    }

    return { ...state }
}

export default tableReducer



